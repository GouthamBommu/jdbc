CREATE TABLE IF NOT EXISTS transaction (ID INT(11) AUTO_INCREMENT, 
					NameOnCard VARCHAR(25), 
                    CardNumber VARCHAR(25), 
                    UnitPrice DECIMAL(10,0),
                    Quantity INT(11),     
                    TotalPrice DECIMAL(10,0),
                    ExpDate VARCHAR(16),
                    CreatedOn DATETIME,
                    CreatedBy VARCHAR(45),
                    PRIMARY KEY (ID));
                    
CREATE TABLE IF NOT EXISTS creditcard_type ( creditcard_type VARCHAR(30),
							   prefix INT(11),
							   length INT(11),
                               PRIMARY KEY (creditcard_type, prefix));

INSERT INTO creditcard_type (creditcard_type, prefix, length ) VALUES ("American Express", 34, 15);
INSERT INTO creditcard_type (creditcard_type, prefix, length ) VALUES ("American Express", 37, 15);
INSERT INTO creditcard_type (creditcard_type, prefix, length ) VALUES ("Mastercard", 51, 16);
INSERT INTO creditcard_type (creditcard_type, prefix, length ) VALUES ("Mastercard", 52, 16);
INSERT INTO creditcard_type (creditcard_type, prefix, length ) VALUES ("Mastercard", 53, 16);
INSERT INTO creditcard_type (creditcard_type, prefix, length ) VALUES ("Mastercard", 54, 16);
INSERT INTO creditcard_type (creditcard_type, prefix, length ) VALUES ("Mastercard", 55, 16);
INSERT INTO creditcard_type (creditcard_type, prefix, length ) VALUES ("Visa", 4, 16);
package com.mcda5510;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;

public class Output {
	
	public static void write(String s) throws IOException
	{
		
		FileWriter fw = null;
		try 
		{
			//String sFile = "C:\\Users\\Goutham\\eclipse-workspace\\JDBC Program\\output\\output.txt";
			String sFile ="./output/output.txt";
			fw = new FileWriter(sFile, true);
			fw.write(s);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			Logg.writeLog(Level.SEVERE, e.toString());
		}
		finally
		{
			if(fw != null)
			{
				fw.close();
			}
		}
	}
}

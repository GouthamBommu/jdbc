package com.mcda5510.logic;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;

import com.mcda5510.Logg;
import com.mcda5510.Output;
import com.mcda5510.connect.DatabaseConnect;
import com.mcda5510.entity.Transaction;

public class BusinessActions {
	
	public static Connection con = DatabaseConnect.getConnection();
	
	public static boolean createTransaction(Transaction t) throws IOException
	{		
		int iCustId = t.getId();
		try
		{
			Statement s = con.createStatement();
			
			ResultSet rs = s.executeQuery("select id from transaction where id ="+iCustId);
			if(! rs.next())
			{
				String sQuery = "insert into transaction (NameonCard, CardNumber, UnitPrice, Quantity, TotalPrice, ExpDate, CreatedOn, CreatedBy)"
						+ "Values(?,?,?,?,?,?,?,?)";
				PreparedStatement preparedStatement = con.prepareStatement(sQuery);
				preparedStatement.setString(1, t.getNameOnCard());
				preparedStatement.setString(2, t.getCardNumber());
				preparedStatement.setDouble(3, t.getUnitPrice());
				preparedStatement.setInt(4, t.getQuantity());		
				preparedStatement.setDouble(5, t.getTotalPrice());
				preparedStatement.setString(6, t.getExpDate());
				preparedStatement.setDate(7, t.getCreatedOn());
				preparedStatement.setString(8, t.getCreatedBy());
				preparedStatement.executeUpdate(); 
			}
			else
			{
				return false;
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Logg.writeLog(Level.SEVERE, ex.toString());
		}
		
		return true;	
	}
	
	public static boolean updateTransaction(Transaction t)
	{
		int iCustId = t.getId();
		try
		{
			Statement s = con.createStatement();
			
			ResultSet rs = s.executeQuery("select id from transaction where id ="+iCustId);
			if(rs.next())
			{
				String sQuery = "insert into transaction (NameonCard, CardNumber, UnitPrice, Quantity, TotalPrice, ExpDate, CreatedOn, CreatedBy)"
						+ "Values(?,?,?,?,?,?,?,?)";
				PreparedStatement preparedStatement = con.prepareStatement(sQuery);
				preparedStatement.setString(1, t.getNameOnCard());
				preparedStatement.setString(2, t.getCardNumber());
				preparedStatement.setDouble(3, t.getUnitPrice());
				preparedStatement.setInt(4, t.getQuantity());		
				preparedStatement.setDouble(5, t.getTotalPrice());
				preparedStatement.setString(6, t.getExpDate());
				preparedStatement.setDate(7, t.getCreatedOn());
				preparedStatement.setString(8, t.getCreatedBy());
				preparedStatement.executeUpdate();
			}
			else
			{
				return false;
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Logg.writeLog(Level.SEVERE, ex.toString());
		}
		
		return true;
	}
	
	public static Transaction getTransaction(int iTransId)
	{
		try
		{
			Transaction TransObj = new Transaction();
			String sQuery = "select * from transaction where id="+iTransId;
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sQuery);
			
			if(rs.next())
			{				
				TransObj.setId(rs.getInt("ID"));
				TransObj.setNameOnCard(rs.getString("NameOnCard"));
				TransObj.setCardNumber(rs.getString("CardNumber"));
				TransObj.setUnitPrice(rs.getDouble("Unitprice"));
				TransObj.setQuantity(rs.getInt("Quantity"));
				TransObj.setTotalPrice(rs.getDouble("TotalPrice"));
				TransObj.setExpDate(rs.getString("ExpDate"));
				TransObj.setCreatedOn(rs.getDate("CreatedOn"));
				TransObj.setCreatedBy(rs.getString("CreatedBy"));
									
				return TransObj;
			}
			else
			{
				Logg.writeLog(Level.INFO, "Transaction with the following ID "+ iTransId + " doesn't exists");
				
				Output.write("\n*************************Operation: Get****************************************\n");
				Output.write("Transaction with the following ID "+ iTransId + " doesn't exists");
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Logg.writeLog(Level.SEVERE, ex.toString());
		}
		
		return null;
	}
	
	public static boolean removeTransaction(int iTransId)
	{
		try
		{
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery("select id from transaction where id ="+iTransId);
			if(rs.next())
			{
				s.executeUpdate("delete from transaction where id="+iTransId);
				return true;
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Logg.writeLog(Level.SEVERE, ex.toString());
		}
		return false;
	}
	

}

package com.mcda5510.connect;

import java.sql.*;
import java.util.logging.Level;

import com.mcda5510.Logg;
public class DatabaseConnect {
	
	private static Connection con = null;
	
	private DatabaseConnect()
	{
		
	}
	
	public static Connection getConnection()
	{
		try
		{
			if(con == null)
			{
				Class.forName("com.mysql.cj.jdbc.Driver");			
				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/goutham?autoReconnect=true&useSSL=false", "root", "Goutham@04");
			}
			
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Logg.writeLog(Level.SEVERE, ex.toString());
		}
		
		return con;		
	}
}
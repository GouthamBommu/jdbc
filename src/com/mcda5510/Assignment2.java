package com.mcda5510;

import com.mcda5510.connect.DatabaseConnect;
import com.mcda5510.entity.Transaction;
import com.mcda5510.logic.BusinessActions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Assignment2 {
	
	public static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	public static Connection con = DatabaseConnect.getConnection();
	
	public static void main(String[] args) throws SQLException, IOException
	{
		int i;
		boolean status = false;
		do 
		{
			System.out.println("Do Transaction: Press 1");
			System.out.println("Update Transaction: Press 2");
			System.out.println("Get Transaction: Press 3");
			System.out.println("Remove Transaction: Press 4");
			
			System.out.println("Enter Option:");
			
			i = Integer.parseInt(br.readLine());
		
			switch(i)
			{
				case 1 : {
							Transaction t = inputData(i);
							int iID = 0;														
							String sName = t.getNameOnCard();
							status = BusinessActions.createTransaction(t);
							
							////////Getting Transaction ID///////
							
							Connection con = DatabaseConnect.getConnection();
							Statement s = con.createStatement();							
							ResultSet rs = s.executeQuery("select id from transaction");
							if(rs.last())
							{
								iID  = rs.getInt("ID");
							}							
							
							/////////////////////
							
							if(status)
							{								
								Logg.writeLog(Level.INFO, "Hello " + sName + "!! Transaction with ID: " + iID + " created successfully");
								
								Output.write("\n**************************Operation: Create***************************************\n");
								Output.write("Hello " + sName + "!! Transaction with ID: " + iID + " created successfully");
							}
							else
							{								
								Logg.writeLog(Level.INFO, "Hello " + sName + "!! Transaction ID: " + iID + " already exists -> Wanna update?");
								
								Output.write("\n***************************Operation: Create**************************************\n");
								Output.write("Hello " + sName + "!! Transaction ID: " + iID + " already exists -> Wanna update?");
							}
				         }
						
						 break;
						 
				case 2 : {
							Transaction t = inputData(i);
							int iID = t.getId();
							String sName = t.getNameOnCard();
							status = BusinessActions.updateTransaction(t);
							if(status)
							{
								Logg.writeLog(Level.INFO, "Hello " + sName + "!!! Transaction with ID: " + iID + " updated successfully");
								
								Output.write("\n****************************Operation: Update*************************************\n");
								Output.write("Hello " + sName + "!!! Transaction with ID: " + iID + " updated successfully");
								
							}
							else
							{								
								Logg.writeLog(Level.INFO, "Hello " + sName + "\nTransaction with ID: " + iID + " doesn't exist -> Create a new one");
								
								Output.write("\n*****************************Operation: Update************************************\n");
								Output.write("Hello " + sName + "\nTransaction with ID: " + iID + " doesn't exist -> Create a new one");
							}
							
						 }
						
						 break;
						 
				case 3 : {
							System.out.println("Enter transaction id: ");
							int iTransId = Integer.parseInt(br.readLine()); 
							Transaction t = BusinessActions.getTransaction(iTransId);
							if(t != null)
							{
								String sTransObj =    "ID: " + t.getId() + "\n"
													+ "NameOnCard: "+ t.getNameOnCard() + "\n" 
													+ "CardNumber: "+ t.getCardNumber() + "\n"
													+ "Unitprice: " + t.getUnitPrice() + "\n"
													+ "TotalPrice: "+ t.getTotalPrice() + "\n"	
													+ "ExpDate: "   + t.getExpDate() + "\n"
													+ "CreatedOn: " + t.getCreatedOn() + "\n"
												    + "CreatedBy: " + t.getCreatedBy() + "\n";											
								System.out.println("************************************");
								System.out.println(sTransObj);
								System.out.println("************************************");
								
								Output.write("\n*************************Operation: Get****************************************\n");
								Output.write(sTransObj);
							}
						 }
						
						 break;
				 
				case 4 : {
							System.out.println("Enter transaction id: ");
							int iTransId = Integer.parseInt(br.readLine()); 	
							boolean bOk = BusinessActions.removeTransaction(iTransId);
							
							if(bOk)
							{
								Logg.writeLog(Level.INFO, "Record " + iTransId + " deleted successfully");
								
								Output.write("\n***************************Operation: Delete**************************************\n");								
								Output.write("Record " + iTransId + " deleted successfully");
							}
							else
							{
								Logg.writeLog(Level.INFO, "Record " + iTransId + " not found!!!");
								
								Output.write("\n***************************Operation: Delete*************************************\n");								
								Output.write("Record " + iTransId + " not found!!!");
							}
					
						 }
						
						 break;
						 
				default : System.out.println("Invalid Input \nTry again!!!");
				
			}
			
			System.out.println("************************************");
			System.out.println("Do you want to continue ? (Y/N)");			
			if(br.readLine().equalsIgnoreCase("Y"))
			{
				status = true;
			}
			else
			{
				status = false;
			}
						
		}while (status);
	}
	
	public static Transaction inputData(int iOption) throws IOException
	{
		
		Transaction t = new Transaction();
		String sValue;
		double dTotalPrice = 0;
		double dUnitPrice = 0;
		int iQuantity = 0;
				
		boolean bOk = true;
		
		if(iOption == 2)
		{
			do
			{
				bOk = true;		
				System.out.println("Transaction ID: ");
				sValue = br.readLine();
				if(isInteger(sValue))
				{
					t.setId(Integer.parseInt(sValue));
				}
				else
				{
					System.out.println("Transaction Id should of integer value");
					bOk = false;
				}
				
			}while(! bOk);
		}
		
		do
		{
			bOk = true;
			System.out.println("Name on Card: ");
			sValue = br.readLine();
			if(validation(sValue))
			{
				t.setNameOnCard(sValue);
			}
			else
			{
				System.out.println("Name on card should n't contain [; : ! @ # $ % ^ * + ? < >]\nEnter value again");
				bOk = false;
			}
			
		} while(! bOk);
		
		
		do
		{
			bOk = true;
			System.out.println("Card Number: ");
			sValue = br.readLine();
			
			if(isInteger(sValue))
			{
				if(cardValidation(sValue))
				{
					t.setCardNumber(sValue);
				}
				else
				{
					System.out.println("Card number is not in correct format");
					bOk = false;
				}
			}
			else
			{
				System.out.println("Card number should consists only digits. \nEnter value again");
				bOk = false;
			}
			
		} while(! bOk);
		
		
		System.out.println("Unit Price: ");	
		t.setUnitPrice(dUnitPrice = Double.parseDouble(br.readLine()));
		
		System.out.println("Quantity: ");
		t.setQuantity(iQuantity = Integer.parseInt(br.readLine()));
		
		do
		{
			bOk = true;
			System.out.println("Expiry Date: MM/YYYY ");
			sValue = br.readLine();
			if(validation(sValue))
			{				
				String sPattern = "(\\d{2})\\/(\\d{4})";
				Pattern Patt = Pattern.compile(sPattern);
				Matcher m = Patt.matcher(sValue);
				
				if(m.find())
				{
					int iMonth = Integer.valueOf(m.group(1));
					
					if(iMonth >= 1 && iMonth <= 12)
					{
						int iYear = Integer.valueOf(m.group(2));
						if(iYear >= 2016 && iYear <= 2031)
						{
							t.setExpDate(sValue);
						}
						else
						{
							System.out.println("The expiry year of your card is not range of 2016 & 2031. \nPlease check and enter again");
							bOk = false;
						}
					}
					else
					{
						System.out.println("Month should be in the range of 01 to 12. \nPlease check and enter again");
						bOk = false;
					}
					
				}	
				else
				{
					System.out.println("The format of the expiration date should be MM/YYYY");
					bOk = false;
				}
			}
			else
			{
				System.out.println("Expiry date should n't contain [; : ! @ # $ % ^ * + ? < >]\nEnter value again");
				bOk = false;
			}
			
		} while(! bOk);
		
		
		dTotalPrice = iQuantity*dUnitPrice;
		t.setTotalPrice(dTotalPrice);
						
		t.setCreatedOn(new Date(System.currentTimeMillis()));
		t.setCreatedBy(System.getProperty("user.name"));
						
		return t;		
	}
	
	public static boolean validation(String s)
	{
		String sPattern = "[;:!@#$%^*+?<>]";
		Pattern Patt = Pattern.compile(sPattern);
		
		if(! Patt.matcher(s).find())
		{
			return true;
		}
		
		return false;
	}
	
	public static boolean cardValidation(String s)
	{
		/*char firstChar = s.charAt(0);
		char secondChar = s.charAt(1);
		
		if(s.length() == 16)
		{
			if(firstChar == '5')
			{
				if(secondChar > '1' && secondChar < '5')
				{
					return true;
				}
				
			}
			else if (firstChar == '4')
			{
				return true;
			}
		}
		else if(s.length() == 15)
		{
			if(firstChar == '3' && (secondChar == '4' || secondChar == '7'))
			{
				return true;
			}
		}			
		
		return false;*/
		try
		{
			if(!s.isEmpty() && s != null)
			{
				char firstChar = s.charAt(0);
				Statement statmnt = con.createStatement();
				ResultSet rs = statmnt.executeQuery("select prefix, length from goutham.creditcard_type where prefix like \'"+ firstChar+ "%\' and length=" + s.length());
				
				while(rs.next())
				{
					String sPrefix = rs.getString("prefix");
					if((sPrefix.length() == 2 && s.substring(0, 2).equalsIgnoreCase(sPrefix)) || 
							(sPrefix.length() == 1 && s.substring(0, 1).equalsIgnoreCase(sPrefix)))
					{
						return true;
					}
				}
			}
			else
			{
				System.out.println("Card number should not be empty");
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Logg.writeLog(Level.SEVERE, ex.toString());
		}
		
		return false;
	}
	
	public static boolean isInteger(String s)
	{
		char[] charArray = s.toCharArray();
		
		for(char c: charArray)
		{
			if(! Character.isDigit(c))
			{
				return false;
			}
		}
		
		return true;
	}

}

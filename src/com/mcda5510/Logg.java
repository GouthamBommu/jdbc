package com.mcda5510;

import java.io.IOException;
import java.util.logging.*;

public class Logg {

	
	public static void writeLog(Level errLvl, String sMsg)
	{		
		try
		{
			Formatter simpleFormatter = new SimpleFormatter();
				
			String sFile ="./logs/Logfile.log";
			Handler fileHandler = null;
			Logger logger = Logger.getLogger(Assignment2.class.getName());
					
			fileHandler = new FileHandler(sFile, true);
			fileHandler.setLevel(Level.ALL);
			fileHandler.setFormatter(simpleFormatter);
			logger.addHandler(fileHandler);
				
					
			logger.log(errLvl, sMsg);
			fileHandler.close();
		
		}
		catch(IOException ex)
		{
			ex.printStackTrace();		
		}
	}
}
